/**
 * @author Sindhu
 * Date: 9-June-2019
 */
package com.todo.test.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import com.todo.model.TodoItem;
import com.todo.model.TodoList;
import com.todo.model.User;
import com.todo.repository.TodoItemRepository;
/**
 * 
 * Repository testCases run directly as Spring Service Test.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TodoRepoIT {
    @Autowired
    TodoItemRepository itemRepository;
    @Autowired
    private TestEntityManager entityManager;
    private User owner;
    private TodoList todoList;

    @Before
    public void setUp() throws Exception {
        owner = entityManager.find(User.class, 1L);
        todoList = entityManager.find(TodoList.class, 1L);
    }

    
    @Test
    public void testFindOneByIdAndListAndOwner() {
        TodoItem todoItem = itemRepository.findOneByIdAndListAndOwner(1L, todoList, owner);
        assertNotNull(todoItem);
    }

    @Test
    public void testDelete() {
    	itemRepository.deleteByIdAndListAndOwner(1L, todoList, owner);
        assertNull(itemRepository.findOne(1L));
    }
    
    @Test
    public void testCreateItem() {
        TodoItem todoItem = new TodoItem(null, todoList, owner);
        todoItem.setName("sindhu");
        todoItem.setList(todoList);
        todoItem.setOwner(owner);
        itemRepository.save(todoItem);

        TodoItem loadedItem = itemRepository.findOne(1L);
        assertThat(loadedItem.getName()).isEqualTo("sindhu");

        TodoList loadedTodoList = entityManager.find(TodoList.class, 1L);
        assertThat(loadedTodoList.getItems()).contains(loadedItem);
    }
	
}
