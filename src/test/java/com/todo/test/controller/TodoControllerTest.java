/**
 * @author Sindhu
 * Date: 9-June-2019
 */
package com.todo.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todo.controller.TodoController;
import com.todo.model.TodoItemRequest;
import com.todo.model.TodoList;
import com.todo.model.TodoListCreatedResponse;
import com.todo.model.TodoListRequest;
import com.todo.model.User;
import com.todo.service.TodoService;

@Category(TodoControllerTest.class)
@RunWith(MockitoJUnitRunner.class)
public class TodoControllerTest {
	private MockMvc mockMvc;

	@InjectMocks
	TodoController controllerMock = new TodoController();
	@Autowired
	private ObjectMapper mapper;
	@Mock
	TodoService todoServiceMock;
	private static final Long MOCK_ID = 1l;
	private static final String MOCK_STR = "1";
	private static final String MOCK_NAME = "sin";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controllerMock).build();
	}

	@Test
	public void testCreate() throws Exception {
		TodoListCreatedResponse response = new TodoListCreatedResponse(MOCK_STR, MOCK_NAME);
		String requestBody = mapper.writeValueAsString(new TodoListRequest("chores"));
		ResponseEntity r = new ResponseEntity(HttpStatus.ACCEPTED);
		when(controllerMock.create(Matchers.any(TodoListRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(r);
		this.mockMvc
				.perform(post("/lists").content(requestBody).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andExpect(content().json(mapper.writeValueAsString(response)));

	}

	@Test
	public void testCreateItem() throws Exception {
		TodoListCreatedResponse response = new TodoListCreatedResponse(MOCK_STR, MOCK_NAME);
		String requestBody = mapper.writeValueAsString(new TodoListRequest("chores"));
		ResponseEntity r = new ResponseEntity(HttpStatus.ACCEPTED);
		when(controllerMock.createItem(MOCK_ID, Matchers.any(TodoItemRequest.class),
				Matchers.any(Authentication.class))).thenReturn(r);
		this.mockMvc
				.perform(post("/lists/{id}/items", "1").content(requestBody).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andExpect(content().json(mapper.writeValueAsString(response)));

	}

	@Test
	@WithUserDetails("sin@todo.com")
	public void testList() throws Exception {
		ResponseEntity r = new ResponseEntity(HttpStatus.OK);
		List<TodoList> results = Arrays.asList(new TodoList(MOCK_NAME, null), new TodoList("nav", null));
		when(controllerMock.list(Matchers.any(Authentication.class))).thenReturn(r);
		this.mockMvc.perform(get("/lists").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(results))).andDo(print());
	}

	@Test
	@WithUserDetails("sin@todo.com")
	public void testGetBasedOnId() throws Exception {
		User owner = new User();
		TodoList todoList = new TodoList(MOCK_NAME, owner);
		ResponseEntity r = new ResponseEntity(HttpStatus.OK);

		when(controllerMock.getBasedOnId(MOCK_ID, Matchers.any(Authentication.class))).thenReturn(r);
		this.mockMvc.perform(get("/lists/{id}", 1L).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(todoList))).andDo(print());
	}

	@Test
	@WithUserDetails("sin@todo.com")
	public void testDelete() throws Exception {
		ResponseEntity r = new ResponseEntity(HttpStatus.NO_CONTENT);
		when(controllerMock.delete(MOCK_ID, Matchers.any(Authentication.class))).thenReturn(r);
		assertEquals(r.getStatusCodeValue(), 204);
		this.mockMvc.perform(delete("/lists/{id}", 1L)).andExpect(status().isNoContent()).andDo(print());
	}

	@Test
	@WithUserDetails("sin@todo.com")
	public void testUpdate() throws Exception {
		ResponseEntity r = new ResponseEntity(HttpStatus.NO_CONTENT);
		TodoListRequest request = new TodoListRequest("Chores");
		String requestBody = mapper.writeValueAsString(request);
		when(controllerMock.update(MOCK_ID, Matchers.any(TodoListRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(r);
		assertEquals(r.getStatusCodeValue(), 204);

		this.mockMvc.perform(put("/lists/{id}", 1L).content(requestBody).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

	}

}