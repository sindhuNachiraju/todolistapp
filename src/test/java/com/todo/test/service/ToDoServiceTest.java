/**
 * @author Sindhu
 * Date: 9-June-2019
 */
package com.todo.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.todo.exception.DataNotFoundException;
import com.todo.exception.TodoServiceException;
import com.todo.model.TodoItem;
import com.todo.model.TodoItemRequest;
import com.todo.model.TodoList;
import com.todo.model.TodoListRequest;
import com.todo.model.User;
import com.todo.repository.TodoItemRepository;
import com.todo.repository.TodoListRepository;
import com.todo.service.TodoService;

/**
 * 
 * Service file TestCases.
 *
 */
@Category(ToDoServiceTest.class)
@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {

	@Mock
	private TodoListRepository listRepository;
	@Mock
	private TodoItemRepository itemRepository;

	@InjectMocks
	TodoService todoService = new TodoService(listRepository, itemRepository);

	private static final Long NUM = 1l;

	private static final Long NUM_ZERO = 0L;

	@Test
	public void testCreateList() {
		TodoList todoList = new TodoList("sin", new User());
		when(todoService.createList(Matchers.any(TodoListRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(todoList);
		assertNotNull(todoList);
	}

	@Test(expected = TodoServiceException.class)
	public void testFindTransactionListException() {
		when(todoService.createList(Matchers.any(TodoListRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(null);
		TodoList todoList = todoService.createList(null, Matchers.any(Authentication.class));
		assertNull(todoList);
	}

	@Test
	public void testListAll() {
		when(todoService.listAll(Matchers.any(Authentication.class))).thenReturn(new ArrayList<TodoList>());
		List<TodoList> list = todoService.listAll(Matchers.any(Authentication.class));
		assertNull(list);
	}

	@Test
	public void testGetBasedOnId() {
		when(todoService.getBasedOnId(NUM, Matchers.any(Authentication.class)))
				.thenReturn(new TodoList("sin", new User()));
		TodoList list = todoService.getBasedOnId(NUM, Matchers.any(Authentication.class));
		assertNull(list);
	}

	@Test
	public void testDelete() {
		Mockito.doNothing().when(todoService.delete(NUM, Matchers.any(Authentication.class)));
	}

	@Test
	public void testUpdateList() {
		TodoList todoList = new TodoList("sin", new User());
		when(todoService.updateList(NUM, Matchers.any(TodoListRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(todoList);
		assertNotNull(todoList);
	}

	@Test
	public void testUpdateItem() {
		TodoItem todoItem = new TodoItem("sin", new TodoList("sin2", new User()), new User());
		when(todoService.updateItem(NUM, NUM, Matchers.any(TodoItemRequest.class), Matchers.any(Authentication.class)))
				.thenReturn(todoItem);
		assertNotNull(todoItem);
	}

}