# To-Do List App
A Simple To-Do List app built with React.js

### Version
1.0.0

### Built Using
- React.js
- React-Router

## Usage

### Clone
Clone the Project

```sh
$ git clone https://sindhuNachiraju@bitbucket.org/sindhuNachiraju/todolistapp.git
$ cd todolist
```

### Installation

Install the dependencies

```sh
$ npm install
```

### Run

This will run your dev server at http://localhost:3000

```sh
$ npm start
```
