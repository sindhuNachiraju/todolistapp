insert into role(id,name) values (1, 'USER');
insert into role(id,name) values (2, 'ano user');

insert into users(id, email, name, password) 
values (1,  'sin@todo.com',  'Sindhu Nachiraju', 'password');
insert into users(id, email,  name, password) values (2, 'suncorp@todo.com',  'Sun Corp', 'passwords');

insert into user_roles(user_id, roles_id) values (1, 1);
insert into user_roles(user_id, roles_id) values (1, 2);
insert into user_roles(user_id, roles_id) values (2, 1);

insert into todo_list(id, name, owner_user_id) values (1, 'first list', 1);
insert into todo_list(id, name, owner_user_id) values (2, 'second list', 1);

insert into todo_item(id, name, todo_list_id, completed, owner_user_id) values (1, 'User 1', 1, FALSE, 1);
insert into todo_item(id, name, todo_list_id, completed, owner_user_id) values (2, 'User 2', 1, FALSE, 1);
insert into todo_item(id, name, todo_list_id, completed, owner_user_id) values (3, 'User 3', 1, TRUE, 1);
