--drop command is coded so that any user can start fresh.
--DROP TABLE role IF EXISTS;

CREATE TABLE role (
  id  INTEGER NOT NULL,
  name VARCHAR(30),
  );
CREATE TABLE user (
  id INTEGER NOT NULL,
  todo_list_id         INTEGER,
  email VARCHAR(30),
    name VARCHAR(30),
      password VARCHAR(30),
  );
  CREATE TABLE user_roles (
  user_id INTEGER NOT NULL,
  roles_id         INTEGER,
 );

  CREATE TABLE todo_list (
  id INTEGER NOT NULL,
  owner_user_id         INTEGER,
  name VARCHAR(30),
  );

CREATE TABLE todo_item (
  id INTEGER NOT NULL,
  todo_list_id         INTEGER,
  name VARCHAR(30),
  completed BOOLEAN
    owner_user_id         INTEGER,
  );

 