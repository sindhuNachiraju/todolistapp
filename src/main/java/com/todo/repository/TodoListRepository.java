/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.todo.model.TodoList;
import com.todo.model.User;
/**
 * 
 * Repository class
 *
 */
@RepositoryRestResource
public interface TodoListRepository extends CrudRepository<TodoList, Long> {
    List<TodoList> findAllByOwner(User owner);

    TodoList findOneByIdAndOwner(Long id, User owner);

    @Transactional
    void deleteByIdAndOwner(Long id, User owner);
}
