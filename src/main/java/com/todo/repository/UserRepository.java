/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.todo.model.User;
/**
 * 
 * Repository class
 *
 */
@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}
