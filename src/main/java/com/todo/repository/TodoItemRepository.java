/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.todo.model.TodoItem;
import com.todo.model.TodoList;
import com.todo.model.User;
/**
 * 
 * Repository class
 *
 */
@RepositoryRestResource
public interface TodoItemRepository extends CrudRepository<TodoItem, Long> {
    TodoItem findOneByIdAndListAndOwner(Long id, TodoList todoList, User owner);

    @Transactional
    void deleteByIdAndListAndOwner(Long id, TodoList todoList, User owner);
}
