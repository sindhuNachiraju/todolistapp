/**
 * @author Sindhu
 * Date: 9-June-2019
 */
package com.todo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.todo.exception.TodoServiceException;
import com.todo.model.TodoItem;
import com.todo.model.TodoItemRequest;
import com.todo.model.TodoList;
import com.todo.model.TodoListRequest;
import com.todo.model.User;
import com.todo.repository.TodoItemRepository;
import com.todo.repository.TodoListRepository;

/**
 * 
 * Service and ServiceIMPL file combined
 *
 */
@Service
public class TodoService {

	final static Logger LOGGER = LoggerFactory.getLogger(TodoService.class);
	private TodoListRepository listRepository;

	private TodoItemRepository itemRepository;

	public TodoService(final TodoListRepository listRepository, final TodoItemRepository itemRepository) {
		this.listRepository = listRepository;
		this.itemRepository = itemRepository;
	}

	public TodoList createList(TodoListRequest todoListRequest, Authentication authentication) {
		TodoList todoList = listRepository
				.save(TodoList.from(todoListRequest, getOwnerFromAuthentication(authentication)));
		if (todoList == null) {
			throw new TodoServiceException("Error while saving TodoList in DB");
		}
		return todoList;

	}

	public TodoItem createItem(Long id, TodoItemRequest todoItemRequest, Authentication authentication) {
		TodoList todoList = listRepository.findOneByIdAndOwner(id, getOwnerFromAuthentication(authentication));
		TodoItem todoItem = itemRepository.save(TodoItem.from(todoItemRequest, todoList));
		if (todoList == null) {
			throw new TodoServiceException("Error while saving TodoItem in DB");
		}
		return todoItem;
	}

	private User getOwnerFromAuthentication(Authentication authentication) {
		return (User) authentication.getPrincipal();
	}

	public List<TodoList> listAll(Authentication authentication) {
		return listRepository.findAllByOwner(getOwnerFromAuthentication(authentication));
	}

	public TodoList getBasedOnId(Long id, Authentication authentication) {
		return listRepository.findOneByIdAndOwner(id, getOwnerFromAuthentication(authentication));
	}

	public void delete(Long id, Authentication authentication) {
		listRepository.deleteByIdAndOwner(id, getOwnerFromAuthentication(authentication));
	}

	public void delete(Long id, Long itemId, Authentication authentication) {
		TodoList todoList = listRepository.findOneByIdAndOwner(id, getOwnerFromAuthentication(authentication));
		itemRepository.deleteByIdAndListAndOwner(itemId, todoList, getOwnerFromAuthentication(authentication));
	}

	public TodoList updateList(Long id, TodoListRequest request, Authentication authentication) {
		TodoList todoList = listRepository.findOneByIdAndOwner(id, getOwnerFromAuthentication(authentication));
		todoList.merge(request);
		listRepository.save(todoList);
		return todoList;

	}

	public TodoItem updateItem(Long id, Long itemId, TodoItemRequest request, Authentication authentication) {
		TodoList todoList = listRepository.findOneByIdAndOwner(id, getOwnerFromAuthentication(authentication));
		TodoItem todoItem = itemRepository.findOneByIdAndListAndOwner(itemId, todoList,
				getOwnerFromAuthentication(authentication));
		todoItem.setName(request.getName());
		todoItem.setCompleted(request.isCompleted());
		itemRepository.save(todoItem);
		return todoItem;

	}

}
