/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.todo.model.TodoItem;
import com.todo.model.TodoItemCreatedResponse;
import com.todo.model.TodoItemRequest;
import com.todo.model.TodoList;
import com.todo.model.TodoListCreatedResponse;
import com.todo.model.TodoListRequest;
import com.todo.model.User;
import com.todo.service.TodoService;
import com.todo.utils.JsonUtils;

/**
 * 
 * Controller class file
 */
@RestController
public class TodoController {
	private final static Logger LOGGER = LoggerFactory.getLogger(TodoController.class);
	@Autowired
	private TodoService todoService;

	@PostMapping("/lists")
	public ResponseEntity<TodoListCreatedResponse> create(@RequestBody TodoListRequest todoListRequest,
			Authentication authentication) {
		TodoList todoList = todoService.createList(todoListRequest, authentication);
		return new ResponseEntity<>(TodoListCreatedResponse.from(todoList), HttpStatus.CREATED);
	}

	@PostMapping("/lists/{id}/items")
	public ResponseEntity<TodoItemCreatedResponse> createItem(@PathVariable("id") Long id,
			@RequestBody TodoItemRequest todoItemRequest, Authentication authentication) {
		TodoItem todoItem = todoService.createItem(id, todoItemRequest, authentication);
		LOGGER.info(JsonUtils.prettyPrint(todoItem));
		return new ResponseEntity<>(TodoItemCreatedResponse.from(todoItem), HttpStatus.CREATED);
	}

	private User getOwnerFromAuthentication(Authentication authentication) {
		return (User) authentication.getPrincipal();
	}

	@GetMapping("/lists")
	public ResponseEntity<Iterable<TodoList>> list(Authentication authentication) {
		List<TodoList> allByOwner = todoService.listAll(authentication);
		return new ResponseEntity<>(allByOwner, HttpStatus.OK);
	}

	@GetMapping("/lists/{id}")
	public ResponseEntity<TodoList> getBasedOnId(@PathVariable("id") Long id, Authentication authentication) {

		return new ResponseEntity<>(todoService.getBasedOnId(id, authentication), HttpStatus.OK);
	}

	@DeleteMapping("/lists/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id, Authentication authentication) {
		todoService.delete(id, authentication);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping("/lists/{id}/items/{itemId}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id, @PathVariable("itemId") Long itemId,
			Authentication authentication) {
		todoService.delete(id, itemId, authentication);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/lists/{id}")
	public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody TodoListRequest request,
			Authentication authentication) {
		todoService.updateList(id, request, authentication);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/lists/{id}/items/{itemId}")
	public ResponseEntity<String> updateItem(@PathVariable("id") Long id, @PathVariable("itemId") Long itemId,
			@RequestBody TodoItemRequest request, Authentication authentication) {
		todoService.updateItem(id, itemId, request, authentication);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
