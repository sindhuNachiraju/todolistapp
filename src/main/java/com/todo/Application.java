/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * Spring Boot Start Up Class
 *
 */
@SpringBootApplication( scanBasePackages = { "com.todo" })
public class Application {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
