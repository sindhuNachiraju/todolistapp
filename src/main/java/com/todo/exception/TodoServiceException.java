/**
 * @author Sindhu
 * Date: 9-June-2019
 */
package com.todo.exception;
/**
 * 
 * Custom Exception file.
 *
 */
public class TodoServiceException extends RuntimeException {

	public enum Level {
		INFO, WARN, ERROR
	}

	private static final long serialVersionUID = 1L;

	private ResponseCode code;

	private String errorMessage;

	private Level level;

	public TodoServiceException() {
		super();
	}

	public TodoServiceException(ResponseCode code) {
		super();
		this.code = code;
	}

	public TodoServiceException(ResponseCode code, String errorMessage) {
		super(errorMessage);
		this.code = code;
		this.errorMessage = errorMessage;
	}

	public TodoServiceException(Level level, ResponseCode code, String errorMessage) {
		super(errorMessage);
		this.level = level;
		this.code = code;
		this.errorMessage = errorMessage;
	}

	public TodoServiceException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public ResponseCode getCode() {
		return code;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setCode(ResponseCode code) {
		this.code = code;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}
}
