/**
 * @author Sindhu
 * Date: 3-August-2019
 */
package com.todo;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 
 * ApplicationProperties for Security if passed through header.
 *
 */
@ConfigurationProperties(ignoreUnknownFields = false, prefix = "todo")
public class ApplicationProperties {
	
	private String apiSalt;
	
	public String getApiSalt() {
		return apiSalt;
	}
	public void setApiSalt(String apiSalt) {
		this.apiSalt = apiSalt;
	}
}
