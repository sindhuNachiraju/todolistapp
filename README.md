# Todo List app

A sample application to view, create , update, delete lists.<br />

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install the software and how to install them

```
Java 8,
Apache maven since maven is used to build the libraries and war file,
Any IDE like Eclipse or STS if required.
```

## Folder Structure

After creation, your project should look like this:

```
todolistapp/
README.md
pom.xml
java.gitignore
maven.gitignore
src/
    
```
Here pom.xml is used for build <br />
src is has actual code, properties, testcases <br />
app folder inside src has UI code.

## Available Scripts

In the project directory, you can run:

### `mvn clean install`

This command helps in downloading all maven dependencies which are library files used while coding.<br>

## Source code files
 - TodoController: controller file which has two methods. Each method has validation for input data, failing which, a custom service exception will be thrown.<br />
 - com.todo: This package contains Interceptor files for Logging. It also has basic config files.<br />
 - H2DataSource: DB config file. <br />
 - com.todo.exception: This package has custom exception files.<br />
 - com.todo.model: Model files means pojo files are included in this package.<br />
 - com.todo.repository: This contains two repo files for two tables.<br />
 - TodoService: This is a service class. This also has basic validations post db data fetch.<br />
 - MyUserDetailsService: This is a user details service class. <br />

## Additional Source comments

- Usually If the project is Big and there are many services involved, It is good to have a Service Interface class and and Service IMPL (implementation class).
- Have a seperate class file for all constants and declare them as public static. Put this in common package so that it is easily understood and also accessible to other class files.
- In this project, I have avoided them because, there only few service methods and no repeated constants.

## Running the tests

For all files, Mockitos are used for code coverage. <br />
Files can be run as @SpringBootTest.

### Break down into end to end tests

 - TodoRepoIT.java: Repository testcases. <br />
 - TodoControllerTest: Controller Testcases. <br />
 - ToDoServiceTest: Mock testcases for service file.<br />

## Deployment
### `mvn build` OR
### execute the command `mvn clean install`
You can observe, Building war:/ in target folder

## Versioning

For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sindhu Nachiraju** - *Initial work* 


## License
Not applicable.<br />
However, we can have LICENSE.md for details related to licensing.

